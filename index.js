var fs = require('fs');
var request = require('request');
var express = require('express');

var app = express();

app.use(express.static('./'));

app.get('/', function(req, res) {
  res.sendFile(__dirname + '/index.html');
});

app.get('/data', async function(req, res) {
  var data = await new Promise(function(resolve) {
    connection.query(`
        SELECT *
        FROM prices
        ORDER BY id DESC
        LIMIT 666
      `, function (error, results, fields) {
        if (error) throw error;

        resolve(results);
      });
  });
  res.json(data);
});

app.get('/data/:id', async function(req, res) {
  var data = await new Promise(function (resolve) {
    connection.query(`
        SELECT *
        FROM prices
        WHERE item_id = ${req.params.id}
      `, function (error, results, fields) {
        if (error) throw error;

        resolve(results);
      });
  });
  res.json(data);
});

app.listen(process.env.PORT || 3000);

var items = JSON.parse(fs.readFileSync('items.json')).items;
// var prices = JSON.parse(fs.readFileSync('prices.json'));

var connection;

(async function() {
  var mysql = require('mysql');
  connection = mysql.createConnection({
    host: 'remotemysql.com',
    user: 'jbyhnUHcaG',
    password: 'UqiEOKvKiD',
    database: 'jbyhnUHcaG'
  });
  await new Promise(function(resolve) {
    connection.connect(resolve);
  });

  var i = 0;
  while (true) {
    // await new Promise(function (resolve) { setTimeout(resolve, 1001); });
    // continue;
    var item = items[i];
    if (!parseInt(item.id)) {
      i++;
      if (i >= items.length) {
        i = 0;
      }
      continue;
    }

    var data = await new Promise(function(resolve) {
      request(`https://api.torn.com/market/${item.id}?selections=bazaar,itemmarket&key=RmvBIEWXIgxtOlsf`, function (err, response, body) {
        resolve(JSON.parse(body));
      });
    });

    await new Promise(function(resolve) {
      connection.query(`
        INSERT INTO prices (item_id, bazaar, itemmarket, updated_at)
        VALUES
          (
            ${item.id},
            '${JSON.stringify(data.bazaar)}',
            '${JSON.stringify(data.itemmarket)}',
            '${(new Date()).getTime()}'
          );
      `, function (error, results, fields) {
        if (error) throw error;
        
        resolve();
      });
    });

    await new Promise(function(resolve) { setTimeout(resolve, 1001); });

    i++;
    if (i >= items.length) {
      i = 0;
    }
  }
})();

setInterval(async function() {
  // return;
  var dataCount = await new Promise(function(resolve) {
    connection.query(`SELECT count(*) as count FROM prices`, function (err, result) {
      resolve(result[0].count);
    });
  });

  var limit = 10000;
  if (dataCount > limit) {
    connection.query(`DELETE FROM prices LIMIT ${dataCount - limit}`, function (err, result) {
      
    });
  }
}, 5000);

return;
(async function() {
  var i = 0;
  while (true) {
    var item = items[i];
    var data = await new Promise(function (resolve) {
      request(`https://api.torn.com/market/${item.id}?selections=bazaar,itemmarket&key=RmvBIEWXIgxtOlsf`, function (err, response, body) {
        resolve(JSON.parse(body));
      });
    });

    var bazaarData = data.bazaar;
    var min = 999999999999;
    for (var key in bazaarData) {
      if (bazaarData[key].cost < min) {
        min = bazaarData[key].cost;
      }
    }

    var itemPrices = [];
    if (prices[item.id]) {
      itemPrices = prices[item.id].prices;
      if (itemPrices.length > 100) prices[item.id].prices = itemPrices.slice(0, 100);
      itemPrices.push({
        price: prices[item.id].price,
        updated_at: prices[item.id].updated_at,
      });
    }
    prices[item.id] = {
      price: min,
      updated_at: (new Date()).getTime(),
      prices: itemPrices,
    };
    fs.writeFileSync('prices.json', JSON.stringify(prices));
    console.log('updated: ' + item.id);
    await new Promise(function (resolve) { setTimeout(resolve, 1001); });

    i++;
    if (i >= items.length) {
      i = 0;
    }
  }
})();