var fs = require('fs');
const scrapeIt = require("scrape-it")

// Promise interface
scrapeIt("https://eski786.pythonanywhere.com/iribuya/prices", {
  categories: {
    listItem: '.panel.panel-default',
    data: {
      title: {
        selector: '.panel-heading',
      },
      items: {
        listItem: 'tr',
        data: {
          id: {
            selector: 'img',
            attr: 'src',
            convert: function(val) {
              return parseInt(val.replace(/[^0-9]/g, ''));
            },
          },
          name: {
            selector: 'td:nth-of-type(2)',
          },
        },
      },
    },
  },
}).then(({ data, response }) => {
  // console.log(`Status Code: ${response.statusCode}`)
  // console.log(data)

  var categories = [];
  var items = [];

  data.categories.forEach(function(category) {
    category.title = category.title.replace('*', '');
    var categoryId = categories.length + 1;
    categories.push({
      id: categoryId,
      name: category.title,
    });
    category.items.forEach(function(item) {
      // console.log(category.title, item);
      items.push({
        category_id: categoryId,
        id: item.id,
        name: item.name,
      });
    });
  });

  fs.writeFileSync('items.json', JSON.stringify({
    categories: categories,
    items: items,
  }));
})